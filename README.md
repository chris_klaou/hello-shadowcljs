# MWE of cljs along side with ES6 modules

You will need shadowcljs: `npm install -g shadow-cljs`
Inside project run: `npm install` and then `shadow-cljs watch app`.
Open your browser at http://localhost:8700/ 

## Run

``` shell
yarn install

yarn watch
```

## Clean

``` shell
yarn clean
```

## Release

``` shell
yarn release
```

## License

Copyright � 2017 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
